package password;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testContainsTwoDigitsRegular() {
		boolean result = PasswordValidator.containsTwoDigits("abc1234abc");
		assertTrue("Invalid number of digits", result);
	}

	@Test
	public void testContainsTwoDigitsException() {
		boolean result = PasswordValidator.containsTwoDigits("abcabcabcabc");
		assertFalse("Invalid number of digits", result);
	}

	@Test
	public void testContainsTwoDigitsBI() {
		boolean result = PasswordValidator.containsTwoDigits("abc12abcabc");
		assertTrue("Invalid number of digits", result);
	}
	
	@Test
	public void testContainsTwoDigitsBO() {
		boolean result = PasswordValidator.containsTwoDigits("abc1abcabc");
		assertFalse("Invalid number of digits", result);
	}
	
	@Test
	public void testIsValidLengthRegular() {
		boolean result = PasswordValidator.isValidLength("1234567890");
		assertTrue("Invalid Length", result);
	}

	@Test
	public void testIsValidLengthException() {
		boolean result = PasswordValidator.isValidLength("1234");
		assertFalse("Invalid Length", result);
	}

	@Test
	public void testIsValidLengthExceptionSpaces() {
		boolean result = PasswordValidator.isValidLength("     test     ");
		assertFalse("Invalid Length", result);
	}

	@Test
	public void testIsValidLengthBI() {
		boolean result = PasswordValidator.isValidLength("12345678");
		assertTrue("Invalid Length", result);
	}

	@Test
	public void testIsValidLengthBO() {
		boolean result = PasswordValidator.isValidLength("1234567");
		assertFalse("Invalid Length", result);
	}

}
